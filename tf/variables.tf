variable "aws_region" {
  default = "us-east-2"
}

variable "cluster_name" {
  default = "devops-application"
  type    = string
}

variable "cluster_instance_types" { 
  default = ["t3.small"]
  type    = list(string)
}

variable "cluster_scale_min" { 
  default = 2
  type    = number
}

variable "cluster_scale_max" { 
  default = 3
  type    = number
}