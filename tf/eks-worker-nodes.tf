#
# EKS Worker Nodes Resources
# IAM roles
# EKS Node Group 
#

resource "aws_iam_role" "devops_node" {
  name = "eks-devops-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "devops-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.devops_node.name
}

resource "aws_iam_role_policy_attachment" "devops-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.devops_node.name
}

resource "aws_iam_role_policy_attachment" "devops-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.devops_node.name
}

resource "aws_eks_node_group" "devops_node_group" {
  cluster_name    = aws_eks_cluster.devops.name
  node_group_name = "devops-node-group"
  node_role_arn   = aws_iam_role.devops_node.arn
  subnet_ids      = aws_subnet.devops[*].id
  instance_types = var.cluster_instance_types

  scaling_config {
    desired_size = var.cluster_scale_min
    max_size     = var.cluster_scale_max
    min_size     = var.cluster_scale_min
  }

  depends_on = [
    aws_iam_role_policy_attachment.devops-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.devops-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.devops-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}