#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#

resource "aws_vpc" "devops" {
  cidr_block = "10.0.0.0/16"

  tags = map(
    "Name", "eks-devops-node",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
  )
}

resource "aws_subnet" "devops" {
  count = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.devops.id

  tags = map(
    "Name", "eks-devops-node",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
  )
}

resource "aws_internet_gateway" "devops" {
  vpc_id = aws_vpc.devops.id

  tags = {
    Name = "eks-devops"
  }
}

resource "aws_route_table" "devops" {
  vpc_id = aws_vpc.devops.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.devops.id
  }
}

resource "aws_route_table_association" "devops" {
  count = 2

  subnet_id      = aws_subnet.devops.*.id[count.index]
  route_table_id = aws_route_table.devops.id
}