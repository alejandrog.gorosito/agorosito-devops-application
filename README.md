Your main goal is to cloudificate the following monolithic application into a 12-factor-app microservice using Docker and k8s (not an explicit requirement).

Keep in mind the service:

* should contain “production ready” configurations
* should scale out n times easily
* should adapt to dynamic configurations
* object storage must be deployed in AWS

After finishing your task, you should provide the following items:

* local configuration manifest for local deployment
* docker images and deployment manifests
* detailed deployment and configuration documentation
* AWS user credentials to review what have you done (Here please use free tier services)
* a few words about your solution. How does it work? Why did you choose to go that way? Why do you think this is the best approach? Would you make any changes to the application? (source code/automation), why?

Please create a GitHub/Gitlab/Bitbucket/CodeCommit public repo and share your solution with us. Repository name must follow this pattern “your-first-name-letter-surname-devops-application”. (e.g) If Nicolas Porta submits a solution, the repo name should be “nporta-devops-application”

Other tasks:

* Set a basic CI/CD flow using the tool you feel most comfortable (add configurations in your repo)
* Use a K8S cluster or another solution that contemplates running containers (add manifests)
  * Tip use a k3s or k0s kubernetes cluster.
* Deploy everything in AWS

Anything that is not specified here is up to your criteria, taking into account all of the points mentioned above. Do not forget to include clear documentation that allows us to start and test the project locally.

We highly recommend you to use AWS Free-tier for deploying everything you need.






# Solution

**The solution for this test** includes:
1. The deployment of all EKS infrastructure using Terraform. 
2. Continuous Delivery with Gitlab CI/CD.
3. Pushin the project image to Docker Hub.
4. Project deployment to Kubernetes.
5. Mounting an EBS volume to the pods in the Kubernetes cluster.

## Requirements

- To run the project you can provision the infrastructure using Terraform by following the guide in [Terraform Section](http://# "Terraform Section").
- An IAM role for EKS and EBS deploy and mounting. [Follow this guide](http://# "Follow this guide").
- A Kubernetes Cluster with users and permissions to deploy projects.
- A CI/CD Tool in case you want to automate the releases. Check the .gitlab-ci.yml file in this repo.


## AIM user permission

In the tf directory of this repo you'll find all the files needed to deploy an Elastic Kubernertes Service, but first you need to provision your AWS user the permission to create all the resources. In total the code will create 18 resources including roles and security groups for the cluster.

To do this first you need to create a Group for the EKS administrator/s:
- Create a group "eksAdministrators".
- Attach the policies:
	- AmazonEC2FullAccess
	- AmazonElasticFileSystemFullAccess
	- AWSCloudFormationFullAccess

Create a new policy and paste the next permissions in the json editor:
	**New policy: eksAdministrators**
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "iam:GetRole",
                "iam:CreateRole",
                "iam:DeleteRole",
                "iam:AttachRolePolicy",
                "iam:PutRolePolicy",
                "ssm:GetParameters",
                "ssm:GetParameter",
                "iam:ListInstanceProfilesForRole",
                "iam:PassRole",
                "iam:DetachRolePolicy",
                "iam:ListAttachedRolePolicies",
                "iam:ListRolePolicies",
                "iam:GetOpenIDConnectProvider",
                "eks:DescribeCluster"
            ],
            "Resource": [
                "arn:aws:iam::<ACCOUNT-ID>:oidc-provider/*",
                "arn:aws:iam::<ACCOUNT-ID>:role/*",
                "arn:aws:ssm:*:<ACCOUNT-ID>:parameter/aws/*",
                "arn:aws:ssm:*::parameter/aws/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "kms:DescribeKey",
                "eks:*",
                "kms:CreateGrant"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": "iam:ListRoles",
            "Resource": "*"
        }
    ]
}
```

## Deploy the infrastructure with Terraform
### Before start
To deploy the infrastruture you'll need to install and configure:
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [AWS Cli / AWS configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)

### Deploy with Terraform
You can find the files for the IaaC definition under tf/ directory. To deploy all the infrastructure needed for this project **you need to change some values in the variables.tf file**.
### AWS Region
```json
    variable "aws_region" {
      default = "us-east-2"
    }
```

### Cluster Name
```json
    variable "cluster_name" {
      default = "devops-application"
      type    = string
    }
```

### Instance type of your worker nodes
```json
    variable "cluster_instance_types" { 
      default = ["t3.small"]
      type    = list(string)
    }
```
> t3.small are running for the test, default is t3.medium for EKS.

### Set the scaling min and max for the Scaling Group

> cluster_scale_min is also the default number of nodes to create.
```json
variable "cluster_scale_min" { 
  default = 2
  type    = number
}
```

```json
variable "cluster_scale_max" { 
  default = 3
  type    = number
}
```

## Apply the IaaC
To run the Terraform code follow the steps:

1. Init Terraform and download the modules and plugins needed.
```bash
    $ terraform init
```
2. Plan
```bash
    $ terraform plan
```
> Terraform will show you all the stack it's going to create.

3. Create the infrastructure
```bash
    $ terraform apply
```
> The promp will ask you form **confirmation**, type "**yes**" to run the deploy.
> This process may take some time to finish (~15 mins.).


# Deploy the code of the project (api)

### Running the application locally:

**1.** Build the image of the project
```bash
    $ docker build -t devops-app:v1 .
```

**2.** Run the project
```bash
    $ docker run --name devops-test -p 8080:3000 devops-app:v1
```
or detached
```bash
    $ docker run -d --name devops-test -p 8080:3000 devops-app:v1
```
> Navigate to your [localhost:8080](http://localhost:8080/) 

### Running in the cluster EKS
To running the application in EKS you'll need to:
* [Install eksctl and configure](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html).
* [Install kubectl and configure](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/)

#### Kubernetes configuration and cli
To run the deployment of the project in the cluster you'll need to [push the docker project to a docker registry](https://docs.docker.com/docker-hub/repos/#pushing-a-docker-container-image-to-docker-hub). 
You'll find the [image of this project in Docker Hub: **alegorosito/devops-application**](https://hub.docker.com/repository/docker/alegorosito/devops-application).

#### To configure kubectl you'll need to run:
```bash
    $ aws eks --region <region-code> update-kubeconfig --name <cluster_name>
```
Then check the connection with the cluster:
```bash
    $ kubectl get nodes
```
> The response need to be information of the nodes you deploy with Terraform.
```bash
    ip-10-0-0-34.us-east-2.compute.internal   Ready    <none>   46m   v1.18.9-eks-d1db3c
    ip-10-0-1-89.us-east-2.compute.internal   Ready    <none>   46m   v1.18.9-eks-d1db3c
```
#### Prepare your cluster storage
We'll use EBS to storage the data of our application. To do that wee need to first apply the Storage and PVC configurations:

> Storage Class: set the EBS filesystem type and provider
> Persistent Volume Claim: allows the deployment pods to claim for the volume needed, in this case the EBS volume.

***Apply Storage Class and Persistent Volume Claim***
```bash
    $ kubectl apply -f k8s/StorageClass.yaml
    $ kubectl apply -f k8s/PersistentVolumeClaim.yaml
```

#### Once then you can deploy the project:

 1. Deployment: Deploy the application with at leats 2 instances of the app in it.
```bash
    $ kubectl apply -f k8s/Deployment.yaml
```
2. Deploy the service
```bash
    $ kubectl apply -f k8s/Service.yaml
```
3. Expose the project with a LoadBalancer.
```bash
    $ kubectl expose deployment/devops-application --port=80 --target-port=80 --name=devops-lb --type=LoadBalancer 
```
4. Set the autoscaler
It use the CPU as the reference metric for scale this project. **You can set the cpu utilization in k8s/Deployment.yaml file**.
And define the min and max replicas in k8s/HorizontalPodAutoscaler.yaml

Apply the HPA to set the autoscaling property.
```bash
    $ kubectl apply -f k8s/HorizontalPodAutoscaler.yaml
```

> Autoscaling: For horizontal autoscaling a [metrics server](https://github.com/kubernetes-sigs/metrics-server/) is deployed in the cluster.

# Current AWS Endpoint

You can access the current deployed api here: [aa4b68c0f0e2841f19fdad42797ab4ea-1957266826.us-east-2.elb.amazonaws.com](aa4b68c0f0e2841f19fdad42797ab4ea-1957266826.us-east-2.elb.amazonaws.com)

# Final thoughts
***How does it work?***
The application takes advantage of the Gitlab and Gitlab CI suite, as well as its connection to Kubernetes clusters to deploy the application.

***Why?***
As it could be found in other tools, Gitlab integrates not only the versioning of the code, but also an interface for monitoring the pipelines, allowing to run different environments and versions. All this controlled by code through the gitlab-ci.

***Changes?***
Currently the application has a pseudo database in json, it would be necessary to add a MongoSQL database and use this data to populate it (in development / stage environments it could be done in each version update).

